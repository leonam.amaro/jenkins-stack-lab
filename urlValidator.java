//Programa Java para verificar se um URL é válido usando o Apache common validator
import org.apache.commons.validator.routines.UrlValidator;

class URLValidator {
    public static boolean UrlValidator(String url)
    {
        /* Obtendo o UrlValidator */
        UrlValidator defaultValidator = new URLValidator();
        return defaultValidator.isValid(url);
    }

    public static void main(String [] args)
    {
        String url = "http://www.gitlab.com/";

        /* Validar um url */
        if (UrlValidator(url))
            System.out.print("O url inserido " + url +" é válido");
        else
            System.out.print("O url inserido " + url +" não é válido");
    }
}